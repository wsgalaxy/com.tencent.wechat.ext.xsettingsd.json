#!/bin/sh
echo "xsettingsd ext"

_file="/app/ext/xsettingsd/xsettingsd"
if [ -f $_file ] 
then
	touch /tmp/.xsettingsd
	$_file -c /tmp/.xsettingsd &
else
	echo "can not find xsettingsd"
fi
